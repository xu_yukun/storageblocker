package me.yukun.storageblocker.handlers;

import org.bukkit.entity.Player;

import me.yukun.storageblocker.Config;
import me.yukun.storageblocker.Methods;

public class MessageHandler {
	private static MessageHandler instance = new MessageHandler();
	private Config config = Config.getInstance();
	private Methods methods = Methods.getInstance();
	private String prefix = config.getMessageString("Messages.Prefix");
	private String blocked = config.getMessageString("Messages.Blocked");

	public static MessageHandler getInstance() {
		return instance;
	}

	public void blocked(Player player) {
		player.sendMessage(methods.color(prefix + blocked));
		return;
	}
}
