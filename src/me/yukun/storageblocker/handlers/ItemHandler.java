package me.yukun.storageblocker.handlers;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.yukun.storageblocker.Config;
import me.yukun.storageblocker.Methods;

public class ItemHandler {
	private static ItemHandler instance = new ItemHandler();
	private Methods methods = Methods.getInstance();
	private Config config = Config.getInstance();

	public static ItemHandler getInstance() {
		return instance;
	}

	public ItemStack makeItem(String itemname) {
		Material material = Material.valueOf(config.getConfigString("StorageBlocker." + itemname + ".ItemType"));
		ItemStack item = new ItemStack(material);
		ItemMeta itemMeta = item.getItemMeta();
		if (config.getConfigString("StorageBlocker." + itemname + ".Name") != null) {
			String name = config.getConfigString("StorageBlocker." + itemname + ".Name");
			itemMeta.setDisplayName(methods.color(name));
		}
		if (config.getConfigStringList("StorageBlocker." + itemname + ".Lore") != null) {
			ArrayList<String> dlore = new ArrayList<String>();
			for (String line : config.getConfigStringList("StorageBlocker." + itemname + ".Lore")) {
				dlore.add(methods.color(line));
			}
			itemMeta.setLore(dlore);
		}
		item.setItemMeta(itemMeta);
		return item;
	}
}