package me.yukun.storageblocker;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Methods {
	private static Methods instance = new Methods();

	public static Methods getInstance() {
		return instance;
	}

	public String color(String msg) {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}

	public String removeColor(String msg) {
		msg = ChatColor.stripColor(msg);
		return msg;
	}

	public Integer getVersion() {
		String ver = Bukkit.getServer().getClass().getPackage().getName();
		ver = ver.substring(ver.lastIndexOf('.') + 1);
		ver = ver.replaceAll("_", "").replaceAll("R", "").replaceAll("v", "");
		return Integer.parseInt(ver);
	}

	public Boolean isInt(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
}
