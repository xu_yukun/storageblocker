package me.yukun.storageblocker.events;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.yukun.storageblocker.Config;
import me.yukun.storageblocker.handlers.ItemHandler;
import me.yukun.storageblocker.handlers.MessageHandler;
import me.yukun.storageblocker.objects.Blocked;

public class PlayerStoreEvents implements Listener {
	private static ArrayList<Blocked> blocked = new ArrayList<Blocked>();
	private static ArrayList<InventoryAction> actions = new ArrayList<InventoryAction>();
	private MessageHandler messages = MessageHandler.getInstance();
	private static ItemHandler itemhandler = ItemHandler.getInstance();
	private static Config config = Config.getInstance();

	public static void setup() {
		for (String itemnames : config.getConfigConfigSection("StorageBlocker")) {
			ItemStack item = itemhandler.makeItem(itemnames);
			ArrayList<InventoryType> invtypes = new ArrayList<InventoryType>();
			for (String types : config.getConfigStringList("StorageBlocker." + itemnames + ".InventoryTypes")) {
				invtypes.add(InventoryType.valueOf(types));
			}
			Blocked block = new Blocked(item, invtypes);
			blocked.add(block);
		}
		actions.add(InventoryAction.PLACE_ALL);
		actions.add(InventoryAction.PLACE_ONE);
		actions.add(InventoryAction.PLACE_SOME);
		actions.add(InventoryAction.SWAP_WITH_CURSOR);
		return;
	}

	@EventHandler
	private void playerStoreEvent(InventoryMoveItemEvent e) {
		if (e.getItem() != null) {
			for (Blocked block : blocked) {
				if (e.getItem().isSimilar(block.getItem())) {
					if (block.getInvTypes().contains(e.getDestination().getType())) {
						e.setCancelled(true);
						if (e.getInitiator().getHolder() instanceof Player) {
							messages.blocked((Player) e.getInitiator().getHolder());
						}
						return;
					}
				}
				continue;
			}
			return;
		}
	}

	@EventHandler
	private void playerStoreEvent(InventoryClickEvent e) {
		if (!e.getWhoClicked().isOp() && !e.getWhoClicked().hasPermission("StorageBlocker.Admin")) {
			if (e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
				for (Blocked block : blocked) {
					if (e.getCurrentItem().isSimilar(block.getItem())) {
						if (block.getInvTypes().contains(e.getView().getTopInventory().getType())) {
							if (e.getClickedInventory() != e.getView().getTopInventory()) {
								e.setCancelled(true);
								messages.blocked((Player) e.getWhoClicked());
								return;
							}
						}
					}
					continue;
				}
			} else if (actions.contains(e.getAction())) {
				for (Blocked block : blocked) {
					if (e.getCursor().isSimilar(block.getItem())) {
						if (block.getInvTypes().contains(e.getClickedInventory().getType())) {
							e.setCancelled(true);
							messages.blocked((Player) e.getWhoClicked());
							return;
						}
					}
					continue;
				}
			}
		}
	}

	@EventHandler
	private void playerDragStoreEvent(InventoryDragEvent e) {
		if (!e.getWhoClicked().isOp() && !e.getWhoClicked().hasPermission("StorageBlocker.Admin")) {
			for (Blocked block : blocked) {
				if (e.getCursor() != null) {
					ItemStack cur = e.getCursor();
					ItemStack blockitem = block.getItem();
					if (cur.isSimilar(blockitem)) {
						if (block.getInvTypes().contains(e.getView().getTopInventory().getType())) {
							e.setCancelled(true);
							messages.blocked((Player) e.getWhoClicked());
							return;
						}
					}
				} else if (e.getOldCursor() != null) {
					ItemStack old = e.getOldCursor();
					ItemStack blockitem = block.getItem();
					if (isSimilar(old, blockitem)) {
						if (block.getInvTypes().contains(e.getView().getTopInventory().getType())) {
							e.setCancelled(true);
							messages.blocked((Player) e.getWhoClicked());
							return;
						}
					}
				} else {
					ArrayList<ItemStack> checks = new ArrayList<ItemStack>();
					checks.addAll(e.getNewItems().values());
					ItemStack check = checks.get(0);
					ItemStack blockitem = block.getItem();
					if (isSimilar(check, blockitem)) {
						if (block.getInvTypes().contains(e.getView().getTopInventory().getType())) {
							e.setCancelled(true);
							messages.blocked((Player) e.getWhoClicked());
							return;
						}
					}
				}
			}
		}
	}

	private Boolean isSimilar(ItemStack old, ItemStack compare) {
		if (old.getType().toString().equals(compare.getType().toString())) {
			if (old.hasItemMeta() && compare.hasItemMeta()) {
				ItemMeta oldMeta = old.getItemMeta();
				ItemMeta compareMeta = compare.getItemMeta();
				if (oldMeta.hasDisplayName() && compareMeta.hasDisplayName()) {
					if (!oldMeta.getDisplayName().equals(compareMeta.getDisplayName())) {
						return false;
					}
				}
				if (oldMeta.hasLore() && compareMeta.hasLore()) {
					if (!oldMeta.getLore().equals(compareMeta.getLore())) {
						return false;
					}
				}
				if (oldMeta.hasEnchants() && compareMeta.hasEnchants()) {
					if (!oldMeta.getEnchants().equals(compareMeta.getEnchants())) {
						return false;
					}
				}
				return true;
			} else {
				return true;
			}
		}
		return false;
	}
}