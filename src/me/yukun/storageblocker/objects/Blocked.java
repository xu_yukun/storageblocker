package me.yukun.storageblocker.objects;

import java.util.ArrayList;

import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class Blocked {
	private ItemStack item;
	private ArrayList<InventoryType> invtypes;

	public Blocked(ItemStack item, ArrayList<InventoryType> invtypes) {
		this.item = item;
		this.invtypes = invtypes;
	}

	public Blocked setItem(ItemStack item) {
		this.item = item;
		return this;
	}

	public Blocked setInvTypes(ArrayList<InventoryType> invtypes) {
		this.invtypes = invtypes;
		return this;
	}

	public ItemStack getItem() {
		return item;
	}

	public ArrayList<InventoryType> getInvTypes() {
		return invtypes;
	}
}