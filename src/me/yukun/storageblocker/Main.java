package me.yukun.storageblocker;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.yukun.storageblocker.events.PlayerStoreEvents;

public class Main extends JavaPlugin implements Listener {
	public static SettingsManager settings;
	private Methods methods;

	@Override
	public void onEnable() {
		settings = SettingsManager.getInstance();
		settings.setup(this);
		methods = Methods.getInstance();
		PluginManager pm = Bukkit.getServer().getPluginManager();
		// ==========================================================================\\
		pm.registerEvents(this, this);
		pm.registerEvents(new PlayerStoreEvents(), this);
		PlayerStoreEvents.setup();
	}

	@EventHandler
	public void authorJoinEvent(PlayerJoinEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if (player.getName().equalsIgnoreCase("xu_yukun")) {
				player.sendMessage(methods.color("&bStore&eBlocker&7 >> &fThis server is using your storage blocker plugin. It is using v" + Bukkit.getServer().getPluginManager().getPlugin("StorageBlocker").getDescription().getVersion() + "."));
			}
		}
	}
}