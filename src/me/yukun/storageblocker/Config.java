package me.yukun.storageblocker;

import java.util.ArrayList;
import java.util.List;

public class Config {
	private static Config instance = new Config();

	public static Config getInstance() {
		return instance;
	}

	public String getConfigString(String path) {
		return Main.settings.getConfig().getString(path);
	}

	public Integer getConfigInt(String path) {
		return Main.settings.getConfig().getInt(path);
	}

	public List<String> getConfigStringList(String path) {
		if (Main.settings.getConfig().getStringList(path) != null) {
			return Main.settings.getConfig().getStringList(path);
		}
		return null;
	}

	public ArrayList<String> getConfigConfigSection(String path) {
		if (Main.settings.getConfig().getConfigurationSection(path) != null) {
			ArrayList<String> section = new ArrayList<String>();
			section.addAll(Main.settings.getConfig().getConfigurationSection(path).getKeys(false));
			return section;
		}
		return null;
	}

	public String getMessageString(String path) {
		return Main.settings.getMessages().getString(path);
	}
}